GENERAL

These data are issued to our study of musical prosody in US-American rap, on the following tracks from the “2001” album: “Still D.R.E.”, “Forgot About Dre”, "The Next Episode”.


CONTENT DESCRIPTION

All files are encoded in ascii into LAB files (or assimilated). The data produced in the study comprise the following analysis into dedicated folders by alphabetical order: 

1) beat (beat)

Musical beats of the musical accompagnement as estimated by the ircamBeat algorithm. 
Data are organized in two columns: 
1: beat time (in second)
2: beat numbers (1, 2, 3 or 4, 4/4 measures, 1 being the downbeat of the measure) 

2) F0 analysis (f0)

Fundamental frequency as estimated by the SWIPEP algorithm from the Acapella tracks.
Data are organized in three columns:
1: analysis time
2: F0 value
3: F0 confidence value (“pitch strength”)

3) Speaker turns (spk)

They contain the  times of the rappers vocal interventions. Datas are organized in three columns.
1: start time of the vocal intervention
2: end time of the vocal intervention
3: name of the speaker (E = Eminem, DD = Dr. Dre, SD= Snoop Dog, Trash = Silence)

4) syllable segmentation (syl)

Manually corrected syllable segmentation with corresponding phonetic labels and prosodic annotations.
Datas are organized in four columns:
1: start time
2: end time
3: phonetic labels of the syllable (XSAMPA)
4: prosodic labels of the syllable (see paper for details)

5) vowel segmentation (vow)

Same as 4) but the onset of the syllable is aligned on the onset of the vowel.

The data are shared under Creative Commons Attribution-NonCommercial 2.0 licence. All use of these data must reference to the aftermentionned paper:

"Olivier Migliore, Nicolas Obin. At the Interface of Speech and Music: A Study of Prosody and Musical Prosody in Rap Music. Speech Prosody, Jun 2018, Poznan, Poland." https://hal.sorbonne-universite.fr/hal-01722009/document

Chill, ‘till the next episode...
